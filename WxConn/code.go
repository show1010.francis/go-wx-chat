package wxConn

const (
	CreatePrivateRoomRespStatusCode = 2015
	CreateGroupRoomRespStatusCode   = 2016
	SendMessageRespStatusCode       = 2011
	RoomListRespStatusCode          = 2012
	RoomDetailRespStatusCode        = 2013
	RoomMessageRespStatusCode       = 2014
	RoomAddMemberRespStatusCode     = 2017
	RoomRemoveMemberRespStatusCode  = 2018
	OffLineMesssageRespStatusCode   = 2010
	ReadedMessageRespStatusCode     = 2020

	UpdProfileImgRespStatusCode = 2031
)

const (
	RoomMemberLive  = 1
	RoomMemberLeave = 4
)

var (
	messageKind = map[int]string{
		0:   "文字訊息",
		100: "圖片",
		200: "語音",
	}
)
