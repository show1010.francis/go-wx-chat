package wxConn

import (
	"encoding/json"
	"go-wx-chat/models"
	"go-wx-chat/service"
)

func UserReceiveMessageTag(userId int64, msg service.UserMsg) {
	if len(msg.MessageIds) == 0 {
		return
	}

	var sendMessage sendMessageResp
	json.Unmarshal(msg.Message, &sendMessage)
	service.ReceiveMessageInsert(msg.MessageIds, userId)
}

func UserOffMessageTag(offLineId int64) {
	if offLineId == 0 {
		return
	}

	OffLineUpdateTag(offLineId, 0, 0)
}

type offLineMessageResp struct {
	StatusCode int                   `json:"statusCode"`
	Data       offLineMessageContent `json:"data"`
}
type offLineMessageContent struct {
	Rooms []models.OffLineRoomMessage `json:"rooms"`
}

func GetOffLineMessage(userId int64) (messageIds []int64, sRespByte []byte, err error) {
	var offLine offLineMessageResp

	offLineMsg, err := service.GetOffLineMessage(userId)
	offLine.StatusCode = OffLineMesssageRespStatusCode
	offLine.Data.Rooms = offLineMsg
	for _, roomMsg := range offLineMsg {
		for _, msg := range roomMsg.Message {
			messageIds = append(messageIds, msg.ID)
		}
	}

	sRespByte, err = json.Marshal(offLine)
	return
}

func FindOffLine(user int64, statusCode int) models.OffLineMsg {
	var offLineMsg models.OffLineMsg
	switch statusCode {
	case UpdProfileImgRespStatusCode:

		offLineMsgs, _ := models.MyRoomDo.FindOffLine(user, statusCode)
		if len(offLineMsgs) == 0 {
			return offLineMsg
		}
		offLineMsg = OffLineProFile(offLineMsgs)
		offLineMsg.ReceiveUserId = user
		offLineMsg.StatusCode = UpdProfileImgRespStatusCode
		lastId := service.OffLineMsgInsert(offLineMsg)
		offLineMsg.ID = lastId
	}

	return offLineMsg
}

func OffLineUpdateTag(id int64, userId int64, statusCode int) error {
	err := models.MyRoomDo.OffLineUpdateTag(id, userId, statusCode)
	return err
}

func OffLineProFile(om []models.OffLineMsg) models.OffLineMsg {
	var resp models.OffLineMsg
	var offLineMsg = make(map[int64]UpdProfileDetail)
	for _, data := range om {
		var msgConet UpdUserImgResp
		json.Unmarshal(data.Message, &msgConet)
		for _, profile := range msgConet.Data.Profiles {
			offLineMsg[profile.UserId] = profile
		}
		OffLineUpdateTag(data.ID, data.ReceiveUserId, data.StatusCode)
	}
	var mergeMsg UpdUserImgResp
	var up UpdProfile
	for _, change := range offLineMsg {
		up.Profiles = append(up.Profiles, change)
	}
	mergeMsg.StatusCode = UpdProfileImgRespStatusCode

	mergeMsg.Data = up

	offByte, _ := json.Marshal(mergeMsg)
	resp.Message = offByte

	return resp
}
