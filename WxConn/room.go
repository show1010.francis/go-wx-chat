package wxConn

import (
	"encoding/json"
	"errors"
	"fmt"
	"go-wx-chat/models"
	"go-wx-chat/pkg/fcm"
	"go-wx-chat/service"
	"time"
)

type CreatePvRoom struct {
	UserID int64 `json:"userId"`
}

type CreatePrivateRoomResp struct {
	StatusCode int                   `json:"statusCode"`
	Data       models.RoomMemberList `json:"data"`
}

func createPrivateRoom(message []byte, user int64) ([]int64, []byte, error) {
	var (
		cPvRoom       CreatePvRoom
		returnPayload CreatePrivateRoomResp
	)

	err := json.Unmarshal(message, &cPvRoom)

	if user == cPvRoom.UserID || cPvRoom.UserID == 0 {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{}, jsonStrPayload, errors.New("userID is 0")
	}

	//檢查邀請私聊對象是否存在
	var otherUser models.User
	if otherUser = service.GetUser(models.User{ID: cPvRoom.UserID}); otherUser.ID == 0 {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{}, jsonStrPayload, errors.New("userID is not found")
	}
	var roomId int64
	if roomId = service.CheckPersonExist(user, cPvRoom.UserID); roomId == 0 {

		roomId, err = service.CreatePrivateRoom(otherUser.Nickname, user, cPvRoom.UserID)
		if err != nil {
			returnPayload.StatusCode = 4444
			jsonStrPayload, _ := json.Marshal(returnPayload)
			return []int64{}, jsonStrPayload, errors.New("private room create error")
		}
	}

	roomMembers, err := service.GetRoomMember(roomId)

	var rMembers []int64
	for _, data := range roomMembers.Members {
		rMembers = append(rMembers, data.UserId)
	}

	if err != nil {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{}, jsonStrPayload, errors.New("get roomMember errors")
	}

	returnPayload.StatusCode = CreatePrivateRoomRespStatusCode
	returnPayload.Data = roomMembers
	jsonStrPayload, err := json.Marshal(returnPayload)
	if err != nil {
		return []int64{}, []byte(""), err
	}

	return rMembers, jsonStrPayload, nil
}

type CreateGroupRoom struct {
	RoomName string  `json:"roomName"`
	Members  []int64 `json:"members"`
}

type CreateGroupRoomResp struct {
	StatusCode int                   `json:"statusCode"`
	Data       models.RoomMemberList `json:"data"`
}

func createGroupRoom(message []byte, user int64) ([]int64, []byte, error) {
	var (
		cGroupRoom    CreateGroupRoom
		returnPayload CreateGroupRoomResp
	)

	err := json.Unmarshal(message, &cGroupRoom)

	if len(cGroupRoom.Members) == 0 {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{user}, jsonStrPayload, nil
	}
	for _, addMember := range cGroupRoom.Members {
		if addMember == user {
			returnPayload.StatusCode = 4444
			jsonStrPayload, _ := json.Marshal(returnPayload)
			return []int64{user}, jsonStrPayload, nil
		}
	}

	roomId, err := service.CreateGroupRoom(cGroupRoom.RoomName, user, cGroupRoom.Members)
	if err != nil {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{user}, jsonStrPayload, nil
	}

	roomMembers, err := service.GetRoomMember(roomId)

	if err != nil {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{user}, jsonStrPayload, nil
	}

	returnPayload.StatusCode = CreateGroupRoomRespStatusCode
	returnPayload.Data = roomMembers
	jsonStrPayload, err := json.Marshal(returnPayload)
	if err != nil {
		return []int64{user}, []byte(""), err
	}

	var rMembers []int64
	for _, data := range roomMembers.Members {
		rMembers = append(rMembers, data.UserId)
	}

	return rMembers, jsonStrPayload, nil
}

type sendMessageReq struct {
	RoomID  int64  `json:"roomId"`
	Kind    int    `json:"kind"`
	Message string `json:"message"`
}

type sendMessageResp struct {
	StatusCode int                `json:"statusCode"`
	ErrMessage string             `json:"errMessage"`
	Data       sendMessageContent `json:"data"`
}

type sendMessageContent struct {
	RoomId    int64  `json:"roomId"`
	MessageId int64  `json:"messageId"`
	Message   string `json:"message"`
	Kind      int    `json:"kind"`
	UserId    int64  `json:"userId"`
	SendTime  int64  `json:"timestamp"`
}

func sendMessage(message []byte, user int64) (rMembers []int64, messageId int64, sRespByte []byte, err error) {
	var (
		sMsg  sendMessageReq
		sResp sendMessageResp
	)

	json.Unmarshal(message, &sMsg)

	if sMsg.Message == "" {
		return
	}
	if _, have := messageKind[sMsg.Kind]; !have {
		sResp.StatusCode = 4444
		sResp.ErrMessage = fmt.Sprintf("訊息種類不存在,%v", sMsg.Kind)
		sRespByte, _ = json.Marshal(sResp)
		return
	}

	if messageId, err = service.SendMessage(sMsg.Kind, sMsg.Message, user, sMsg.RoomID); err != nil {
		return
	}

	roomMembers, err := service.GetRoomMember(sMsg.RoomID)

	if err != nil {
		return
	}

	for _, data := range roomMembers.Members {
		rMembers = append(rMembers, data.UserId)
	}

	sResp.StatusCode = SendMessageRespStatusCode
	sResp.Data.RoomId = sMsg.RoomID
	sResp.Data.MessageId = messageId
	sResp.Data.Message = sMsg.Message
	sResp.Data.Kind = sMsg.Kind
	sResp.Data.UserId = user
	sResp.Data.SendTime = time.Now().Unix()

	sRespByte, _ = json.Marshal(sResp)

	go pushNotifi(roomMembers, user, "title", sResp.Data.Message)
	return

}

type readedMessageReq struct {
	RoomId     int64   `json:"roomId"`
	MessageIds []int64 `json:"messageIds"`
}

type readedMessageResp struct {
	StatusCode int                  `json:"statusCode"`
	ErrMessage string               `json:"errMessage"`
	Data       readedMessageContent `json:"data"`
}

type readedMessageContent struct {
	RoomId   int64                 `json:"roomId"`
	Messages []service.ReadMessage `json:"messages"`
}

func readedMessage(message []byte, user int64) (rMembers []int64, sRespByte []byte, err error) {
	var (
		sMsg  readedMessageReq
		sResp readedMessageResp
	)

	json.Unmarshal(message, &sMsg)

	if len(sMsg.MessageIds) == 0 {
		sResp.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(sResp)
		return []int64{user}, jsonStrPayload, nil
	}

	//紀錄己讀資訊
	readMessageInfo := service.ReadedMessage(sMsg.RoomId, sMsg.MessageIds, user)

	roomMembers, err := service.GetRoomMember(sMsg.RoomId)

	if err != nil {
		return
	}

	for _, data := range roomMembers.Members {
		rMembers = append(rMembers, data.UserId)
	}

	sResp.StatusCode = ReadedMessageRespStatusCode
	sResp.Data.RoomId = sMsg.RoomId
	sResp.Data.Messages = readMessageInfo

	sRespByte, _ = json.Marshal(sResp)

	return

}

type getRoomListResp struct {
	StatusCode int      `json:"statusCode"`
	Data       roomList `json:"data"`
}

type roomList struct {
	Rooms []roomListConetnet `json:"rooms"`
}

type roomListConetnet struct {
	RoomId      int64  `json:"roomId"`
	RoomType    int    `json:"type"`
	RoomName    string `json:"name"`
	RoomImg     string `json:"roomImg"`
	LastMsgTime int64  `json:"lastTimestamp"`
}

func getRoomList(userID int64) ([]byte, error) {
	var resp getRoomListResp

	resp.StatusCode = RoomListRespStatusCode

	dbRList, err := service.GetRoomBySelf(userID)
	if err != nil {
		return []byte(""), err
	}

	var respRList = make([]roomListConetnet, 0)

	for _, data := range dbRList {
		respRList = append(respRList, roomListConetnet{
			RoomId:      data.ID,
			RoomType:    data.Type,
			RoomName:    data.Name,
			RoomImg:     "",
			LastMsgTime: data.LastSendTimeStamp,
		})
	}

	var rooms roomList
	rooms.Rooms = respRList

	resp.Data = rooms

	respByte, err := json.Marshal(resp)

	return respByte, err
}

type sendRoomDetailReq struct {
	RoomID int64 `json:"roomId"`
}

type sendRoomDetailResp struct {
	StatusCode int                       `json:"statusCode"`
	Data       sendRoomDetailRespContent `json:"data"`
}

type sendRoomDetailRespContent struct {
	RoomId   int64               `json:"roomId"`
	RoomType int                 `json:"type"`
	RoomName string              `json:"name"`
	RoomImg  string              `json:"roomImg"`
	Members  []models.RoomMember `json:"members"`
}

func GetRoomDetail(message []byte) ([]byte, error) {
	var (
		sMsg       sendRoomDetailReq
		sResp      sendRoomDetailResp
		newMembers []models.RoomMember
	)

	json.Unmarshal(message, &sMsg)

	roomMember, err := service.GetRoomMember(sMsg.RoomID)

	//過濾存在的成員
	for _, nowMember := range roomMember.Members {
		if nowMember.Status == RoomMemberLive {
			newMembers = append(newMembers, nowMember)
		}
	}

	sResp.StatusCode = RoomDetailRespStatusCode
	sResp.Data.RoomId = roomMember.RoomId
	sResp.Data.RoomType = roomMember.RoomType
	sResp.Data.RoomName = roomMember.RoomName
	sResp.Data.Members = newMembers

	sRespByte, _ := json.Marshal(sResp)

	return sRespByte, err
}

type roomMessageReq struct {
	RoomID        int64 `json:"roomId"`
	SrhCreateTime int64 `json:"timestamp"`
}

type roomMessageResp struct {
	StatusCode int                    `json:"statusCode"`
	Data       roomMessageRespContent `json:"data"`
}

type roomMessageRespContent struct {
	RoomId   int64                `json:"roomId"`
	Messages []models.RoomMessage `json:"messages"`
}

func GetRoomMessage(message []byte) ([]byte, error) {
	var (
		sMsg  roomMessageReq
		sResp roomMessageResp
	)

	json.Unmarshal(message, &sMsg)

	if sMsg.RoomID == 0 {
		return []byte(""), errors.New("roomid is 0")
	}

	roomMsg, err := service.RoomMessage(sMsg.RoomID, sMsg.SrhCreateTime)

	sResp.StatusCode = RoomMessageRespStatusCode
	sResp.Data.RoomId = sMsg.RoomID
	sResp.Data.Messages = roomMsg

	sRespByte, _ := json.Marshal(sResp)

	return sRespByte, err
}

type addGroupMemberReq struct {
	RoomID     int64   `json:"roomId"`
	UserIdList []int64 `json:"members"`
}

type addGroupMemberResp struct {
	StatusCode int                   `json:"statusCode"`
	Data       addGroupMemberContent `json:"data"`
}

type addGroupMemberContent struct {
	RoomId     int64               `json:"roomId"`
	RoomName   string              `json:"name"`
	AddMembers []models.RoomMember `json:"addMembers"`
}

func AddGroupMember(message []byte, userId int64) ([]int64, []byte, error) {
	var (
		sMsg         addGroupMemberReq
		sResp        addGroupMemberResp
		sRespContent addGroupMemberContent
		members      []models.RoomMember
		addMember    = make(map[int64]int)
	)

	json.Unmarshal(message, &sMsg)

	roomMembers, err := service.GetRoomMember(sMsg.RoomID)

	for _, nowMember := range roomMembers.Members {
		for _, uid := range sMsg.UserIdList {
			if nowMember.UserId == uid && nowMember.Status == RoomMemberLive {
				sResp.StatusCode = 4444
				jsonStrPayload, _ := json.Marshal(sResp)
				return []int64{userId}, jsonStrPayload, nil
			}
		}
		addMember[nowMember.UserId] = nowMember.Status
	}

	for _, userId := range sMsg.UserIdList {
		switch addMember[userId] {
		case RoomMemberLeave:
			if err := service.RoomUpdMember(sMsg.RoomID, userId, RoomMemberLive); err != nil {
				return []int64{}, []byte(""), err
			}
		default:
			if err := service.RoomAddMember(sMsg.RoomID, userId); err != nil {
				return []int64{}, []byte(""), err
			}
		}
		userData := service.GetUser(models.User{ID: userId})
		members = append(members, models.RoomMember{
			UserId:   userId,
			UserName: userData.Username,
		})
	}

	roomMembers, err = service.GetRoomMember(sMsg.RoomID)

	if err != nil {
		return []int64{}, []byte(""), err
	}

	var rMembers []int64
	for _, data := range roomMembers.Members {
		if data.Status == RoomMemberLive {
			rMembers = append(rMembers, data.UserId)
		}
	}

	sRespContent.RoomId = sMsg.RoomID
	sRespContent.RoomName = roomMembers.RoomName
	sRespContent.AddMembers = members

	sResp.StatusCode = RoomAddMemberRespStatusCode
	sResp.Data = sRespContent

	sRespByte, _ := json.Marshal(sResp)

	return rMembers, sRespByte, err
}

type removeGroupMemberReq struct {
	RoomID     int64   `json:"roomId"`
	UserIdList []int64 `json:"members"`
}

type removeGroupMemberResp struct {
	StatusCode int                      `json:"statusCode"`
	Data       removeGroupMemberContent `json:"data"`
}

type removeGroupMemberContent struct {
	RoomId        int64               `json:"roomId"`
	RoomName      string              `json:"name"`
	RemoveMembers []models.RoomMember `json:"delMembers"`
}

func RemoveGroupMember(message []byte, userId int64) ([]int64, []byte, error) {
	var (
		sMsg         removeGroupMemberReq
		sResp        removeGroupMemberResp
		sRespContent removeGroupMemberContent
		members      []models.RoomMember
	)

	json.Unmarshal(message, &sMsg)

	roomMembers, err := service.GetRoomMember(sMsg.RoomID)
	var rMembers []int64
	for _, data := range roomMembers.Members {
		if data.Status == RoomMemberLive {
			rMembers = append(rMembers, data.UserId)
		}
	}

	//先檢查欲刪除成員 是否皆在此聊天室 且 都是存在的
	for _, nowMember := range roomMembers.Members {
		for _, uid := range sMsg.UserIdList {
			if nowMember.UserId == uid && nowMember.Status != RoomMemberLive {
				sResp.StatusCode = 4444
				jsonStrPayload, _ := json.Marshal(sResp)
				return []int64{userId}, jsonStrPayload, nil
			}
		}
	}

	//對刪除成員 進行狀態tag
	for _, userId := range sMsg.UserIdList {
		if err := service.RoomUpdMember(sMsg.RoomID, userId, RoomMemberLeave); err != nil {
			return []int64{}, []byte(""), err
		}
		userData := service.GetUser(models.User{ID: userId})
		members = append(members, models.RoomMember{
			UserId:   userId,
			UserName: userData.Username,
		})
	}

	sRespContent.RoomId = sMsg.RoomID
	sRespContent.RoomName = roomMembers.RoomName
	sRespContent.RemoveMembers = members

	sResp.StatusCode = RoomRemoveMemberRespStatusCode
	sResp.Data = sRespContent

	sRespByte, _ := json.Marshal(sResp)

	return rMembers, sRespByte, err
}

func pushNotifi(room models.RoomMemberList, sendUser int64, title, message string) {
	//預計要接收的使用者
	for _, member := range room.Members {
		//傳送訊息人員不接收推播
		if member.UserId == sendUser {
			continue
		}
		var title string //推播標題 私人訊息為對方姓名 群組房則為群組房名稱

		if room.RoomType == models.PrivateRoom {
			sendUserModel := service.GetUser(models.User{ID: sendUser})
			title = sendUserModel.Nickname
		} else if room.RoomType == models.GroupRoom {
			title = room.RoomName
		}

		//找出該使用者 全部有註冊的裝置token
		deviceTokenList := service.FindUserDevice(member.UserId)

		for _, d := range deviceTokenList {
			if ok := fcm.Send(fcm.FcmSendBody{
				DeviceToken: d.DeviceToken,
				DeviceType:  d.DeviceType,
				Title:       title,
				Content:     message,
				RoomId:      room.RoomId,
				RoomType:    room.RoomType,
				SendUser:    sendUser,
			}); !ok {
				//傳送失敗 表示token 失敗 刪除
				service.DelUserDevice(d.DeviceToken)
			}
		}
	}
}
