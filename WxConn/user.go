package wxConn

import (
	"encoding/json"
	"go-wx-chat/service"
)

type UpdUserImg struct {
	HeadImg string `json:"headImg"`
}

type UpdUserImgResp struct {
	StatusCode int        `json:"statusCode"`
	Data       UpdProfile `json:"data"`
}

type UpdProfile struct {
	Profiles []UpdProfileDetail `json:"profile"`
}

type UpdProfileDetail struct {
	UserId  int64  `json:"userId"`
	HeadImg string `json:"headImg"`
}

func updUserImg(message []byte, user int64) ([]int64, []byte, error) {
	var (
		cUpdUser      UpdUserImg
		returnPayload UpdUserImgResp
	)

	err := json.Unmarshal(message, &cUpdUser)

	if len(cUpdUser.HeadImg) == 0 {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{user}, jsonStrPayload, nil
	}

	if err := service.UpdateUserHeadImg(user, cUpdUser.HeadImg); err != nil {
		returnPayload.StatusCode = 4444
		jsonStrPayload, _ := json.Marshal(returnPayload)
		return []int64{user}, jsonStrPayload, nil
	}
	var up UpdProfile

	detail := UpdProfileDetail{
		UserId:  user,
		HeadImg: cUpdUser.HeadImg,
	}

	up.Profiles = append(up.Profiles, detail)

	contactFriends, err := service.FindContactUserId(user)
	if err != nil {
		return []int64{user}, []byte(""), err
	}

	returnPayload.StatusCode = UpdProfileImgRespStatusCode
	returnPayload.Data = up
	jsonStrPayload, err := json.Marshal(returnPayload)
	if err != nil {
		return []int64{user}, []byte(""), err
	}

	var rMembers []int64
	var isHaveSelf bool //判斷是否會回傳給自己 沒有則新增
	for _, data := range contactFriends {
		if data.UserId == user {
			isHaveSelf = true
		}
		rMembers = append(rMembers, data.UserId)
	}

	if !isHaveSelf {
		rMembers = append(rMembers, user)
	}

	return rMembers, jsonStrPayload, nil
}
