package wxConn

import (
	"encoding/json"
	"go-wx-chat/models"
	"go-wx-chat/service"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

//clientWS 每一個連線基本資訊
type clientWS struct {
	Hub       *WxManager
	User      string
	Token     string
	Uid       int64
	WSConn    *websocket.Conn
	WriteChan chan service.UserMsg
}

//BroadCastMsg websocket廣播
type BroadCastMsg struct {
	User  []int64
	BCMSG service.UserMsg
}

//WxManager 聊天室訊息中心
type WxManager struct {
	// Register requests from the clients.
	register chan *clientWS
	// Unregister requests from clients.
	unregister   chan *clientWS
	AllRoom      map[int][]string     //room ->userList
	AllToken     map[string]*clientWS //token ->conn
	AllTokenUser map[string]string    //token ->user
	Broadcast    chan BroadCastMsg
	OffLineMsg   chan service.UserMsg
}

var (
	rndseed   = rand.NewSource(time.Now().UnixNano())
	rnd       = rand.New(rndseed)
	tableStop = map[string]chan bool{}
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

func NewWSMSGManage(off chan service.UserMsg) *WxManager {

	return &WxManager{
		Broadcast:    make(chan BroadCastMsg),
		register:     make(chan *clientWS),
		unregister:   make(chan *clientWS),
		AllToken:     make(map[string]*clientWS),
		AllTokenUser: make(map[string]string),
		OffLineMsg:   off,
	}
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
} // use default options

//WebSocket Client Connection
func Wshandler(w http.ResponseWriter, r *http.Request, user string, wx *WxManager, uid int64, tokeID string) {

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}

	writeMSG := make(chan service.UserMsg)
	var whoWS = &clientWS{
		WSConn:    conn,
		Hub:       wx,
		User:      user,
		Uid:       uid,
		Token:     tokeID,
		WriteChan: writeMSG,
	}

	//防止 同一個token 不同連線
	//同一個token 踢掉之前連線
	msgMaplock.Lock()
	if oldWx, have := wx.AllToken[tokeID]; have {
		oldWx.WSConn.Close()
		delete(wx.AllToken, tokeID)     //token列表移除
		delete(wx.AllTokenUser, tokeID) //token <->使用
	}
	msgMaplock.Unlock()

	wx.register <- whoWS

	go whoWS.writeMSG()
	go whoWS.readMSG()
	go service.LogWx(models.LogWx{
		UserId:  uid,
		Action:  service.WxCoon,
		Message: []byte(r.RemoteAddr),
	})
	//登入後傳送離線期間 未接收的離線訊息
	if msgIds, sRespByte, err := GetOffLineMessage(uid); err == nil {
		whoWS.WriteChan <- service.UserMsg{MessageIds: msgIds, Message: sRespByte}
	}

	//登入後傳送離線期間 更新的好友個人資訊
	if profileMsg := FindOffLine(uid, UpdProfileImgRespStatusCode); profileMsg.ID != 0 {
		whoWS.WriteChan <- service.UserMsg{OffLineId: profileMsg.ID, Message: profileMsg.Message}
	}

}

func (w *clientWS) writeMSG() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		w.WSConn.Close()
		w.Hub.unregister <- w
		go service.LogWx(models.LogWx{
			UserId: w.Uid,
			Action: service.WxClose,
		})
		recover()
	}()
	for {
		select {
		case message := <-w.WriteChan:
			go service.LogWx(models.LogWx{
				UserId:  w.Uid,
				Action:  service.WxSend,
				Message: message.Message,
			})
			w.WSConn.SetWriteDeadline(time.Now().Add(writeWait))
			err := w.WSConn.WriteMessage(websocket.TextMessage, message.Message)
			if err != nil {
				log.Println("write:", err)
				return
			}

			//send message db tag
			go UserReceiveMessageTag(w.Uid, message)
			go UserOffMessageTag(message.OffLineId)
		case <-ticker.C:
			w.WSConn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := w.WSConn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

type ReceivedPayload struct {
	Action string `json:"action,omitempty"`
}

func (w *clientWS) readMSG() {
	w.WSConn.SetReadDeadline(time.Now().Add(pongWait))
	w.WSConn.SetPongHandler(func(string) error { w.WSConn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	defer func() {
		w.WSConn.Close()
		w.Hub.unregister <- w
		go service.LogWx(models.LogWx{
			UserId: w.Uid,
			Action: service.WxClose,
		})
		recover()
	}()
	for {
		_, message, err := w.WSConn.ReadMessage()
		go service.LogWx(models.LogWx{
			UserId:  w.Uid,
			Action:  service.WxRecive,
			Message: message,
		})
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			return
		}

		var request ReceivedPayload
		json.Unmarshal(message, &request)

		w.workeverything(request.Action, message)
	}
}

var msgMaplock sync.Mutex

func (wx *WxManager) MSGDispatch() {
	for {
		select {
		case data := <-wx.Broadcast:

			//room廣播
			msgMaplock.Lock()

			//room 內的所有玩家
			for _, user := range data.User {
				is_send := false
				//當前線上 在這room內的玩家發送訊息
				for _, ct := range wx.AllToken {
					if ct.Uid == user {
						is_send = true
						select {
						case ct.WriteChan <- data.BCMSG:
						case <-time.After(time.Second * 1):
							log.Println("room廣播 default close", ct.User)
							close(ct.WriteChan)
							delete(wx.AllToken, ct.Token)     //token列表移除
							delete(wx.AllTokenUser, ct.Token) //token <->使用
						}
					}
				}
				if !is_send {
					data.BCMSG.ReceiveUserId = user
					wx.OffLineMsg <- data.BCMSG
				}
			}

			msgMaplock.Unlock()

		case data := <-wx.register:
			msgMaplock.Lock()
			wx.AllToken[data.Token] = data          //Token <->conn列表增加
			wx.AllTokenUser[data.Token] = data.User //Token <->使用者列表增加
			msgMaplock.Unlock()
		case data := <-wx.unregister:
			msgMaplock.Lock()
			//符合的話，在token <->conn 列表 將連線中斷
			if _, ok := wx.AllToken[data.Token]; ok {
				delete(wx.AllToken, data.Token)     //token列表移除
				delete(wx.AllTokenUser, data.Token) //token <->使用
				close(data.WriteChan)
			}
			msgMaplock.Unlock()

		}
	}
}

type commonResp struct {
	StatusCode int `json:"statusCode"`
}

func (data *clientWS) workeverything(cmd string, msg []byte) {
	switch cmd {
	case "ping":
		var common commonResp
		resultByte, _ := json.Marshal(common)
		data.WriteChan <- service.UserMsg{Message: resultByte}
	case "createPersonalRoom":
		reciveMember, Msg, err := createPrivateRoom(msg, data.Uid)

		if err == nil {
			data.Hub.Broadcast <- BroadCastMsg{
				User:  reciveMember,
				BCMSG: service.UserMsg{Message: Msg},
			}
			return
		}

		data.WriteChan <- service.UserMsg{Message: Msg}
	case "createGroupRoom":
		reciveMember, Msg, err := createGroupRoom(msg, data.Uid)
		if err == nil {
			data.Hub.Broadcast <- BroadCastMsg{
				User:  reciveMember,
				BCMSG: service.UserMsg{Message: Msg},
			}
		}
	case "roomDetail":
		if Msg, err := GetRoomDetail(msg); err == nil {
			data.WriteChan <- service.UserMsg{Message: Msg}
		}
	case "sendMessage":
		reciveMember, messageId, Msg, err := sendMessage(msg, data.Uid)
		if err == nil {
			data.Hub.Broadcast <- BroadCastMsg{
				User:  reciveMember,
				BCMSG: service.UserMsg{Message: Msg, MessageIds: []int64{messageId}, StatusCode: SendMessageRespStatusCode},
			}

		}
	case "readMessage":
		reciveMember, Msg, err := readedMessage(msg, data.Uid)
		if err == nil {
			data.Hub.Broadcast <- BroadCastMsg{
				User:  reciveMember,
				BCMSG: service.UserMsg{Message: Msg, StatusCode: SendMessageRespStatusCode},
			}
		}
	case "roomList":
		resultByte, _ := getRoomList(data.Uid)
		data.WriteChan <- service.UserMsg{Message: resultByte}
	case "roomMessage":
		resultByte, _ := GetRoomMessage(msg)
		data.WriteChan <- service.UserMsg{Message: resultByte}
	case "addGroupMember":
		reciveMember, Msg, err := AddGroupMember(msg, data.Uid)
		if err == nil {
			data.Hub.Broadcast <- BroadCastMsg{
				User:  reciveMember,
				BCMSG: service.UserMsg{Message: Msg},
			}
		}
	case "removeGroupMember":
		reciveMember, Msg, err := RemoveGroupMember(msg, data.Uid)

		if err == nil {
			data.Hub.Broadcast <- BroadCastMsg{
				User:  reciveMember,
				BCMSG: service.UserMsg{Message: Msg},
			}
		}
	case "updateProfile":
		reciveMember, Msg, err := updUserImg(msg, data.Uid)

		if err == nil {
			data.Hub.Broadcast <- BroadCastMsg{
				User:  reciveMember,
				BCMSG: service.UserMsg{Message: Msg, StatusCode: UpdProfileImgRespStatusCode},
			}
		}
	default:
	}

}
