package apis

import (
	"go-wx-chat/service"
	"go-wx-chat/toolkits"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AddDevice(c *gin.Context) {
	uid, _ := c.Get("uid") //token user id
	var deviceToken string
	var deviceType int
	deviceToken = c.PostForm("deviceToken")
	if c.PostForm("deviceType") != "" {
		deviceType, _ = strconv.Atoi(c.PostForm("deviceType"))
	}
	if deviceToken == "" {
		c.JSON(http.StatusOK, gin.H{
			"statusCode": 1,
			"message":    "param is null",
		})
		return
	}

	if err := service.CreateUserNewDevice(deviceToken, toolkits.FToint(uid), deviceType); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"statusCode": 1,
			"message":    err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"statusCode": 0,
		"message":    "裝置註冊成功",
		"data": gin.H{
			"deviceToken": deviceToken,
			"userId":      uid,
		},
	})
}

func DelUserDevice(c *gin.Context) {

	var deviceToken string
	deviceToken = c.PostForm("deviceToken")
	if deviceToken == "" {
		c.JSON(http.StatusOK, gin.H{
			"statusCode": 1,
			"message":    "param is null",
		})
		return
	}

	if err := service.DelUserDevice(deviceToken); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"statusCode": 1,
			"message":    err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"statusCode": 0,
		"message":    "刪除裝置成功",
	})
}
