package apis

import (
	"go-wx-chat/models"
	"go-wx-chat/service"
	"go-wx-chat/toolkits"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AddFriend(c *gin.Context) {
	uid, _ := c.Get("uid") //token user id
	var afData service.Friend

	afData.FriendId, _ = strconv.ParseInt(c.PostForm("friendUserId"), 10, 64)
	afData.UserId = toolkits.FToint(uid)

	if err := service.AddFriend(afData); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"statusCode": 1,
			"message":    err.Error(),
		})
		return
	}

	dbFriend := service.GetUser(models.User{ID: afData.FriendId})

	c.JSON(http.StatusOK, gin.H{
		"statusCode": 0,
		"message":    "新增好友成功",
		"data":       dbFriend,
	})
}

func FindFriendList(c *gin.Context) {

	uid, _ := c.Get("uid") //token user id
	userId := toolkits.FToint(uid)

	friendList := service.FindFriends(userId)

	c.JSON(http.StatusOK, gin.H{
		"message":    "",
		"statusCode": 0,
		"data":       friendList,
	})
}

func DelFriend(c *gin.Context) {
	uid, _ := c.Get("uid") //token user id
	var afData service.Friend
	afData.FriendId, _ = strconv.ParseInt(c.PostForm("friendUserId"), 10, 64)
	afData.UserId = toolkits.FToint(uid)

	if err := service.DelFriends(afData.UserId, afData.FriendId); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"statusCode": 1,
			"message":    err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"statusCode": 0,
		"message":    "刪除好友成功",
	})
}
