package apis

import (
	"go-wx-chat/models"
	"go-wx-chat/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type loginResp struct {
	DateRegist int    `json:"dateOfRegistration"`
	Token      string `json:"token"`
	UserId     int64  `json:"userId"`
	UserName   string `json:"username"`
	HeadImg    string `json:"headImg" ` //圖像
}

func CheckAndToken(c *gin.Context) {

	var uData service.ClientUser
	uData.LoginAcc = c.PostForm("username")
	uData.LoginPwd = c.PostForm("password")

	var lResp loginResp
	//驗證會員登入
	err, loginU := service.CheckLogin(uData.LoginAcc, uData.LoginPwd)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message":    err.Error(),
			"statusCode": 1,
			"data":       lResp,
		})
		return
	}

	//成功後 返還Token
	token, _ := models.SignToken(loginU.Username, loginU.ID)

	lResp.UserId = loginU.ID
	lResp.Token = token
	lResp.UserName = loginU.Username
	lResp.HeadImg = loginU.HeadImg
	c.JSON(http.StatusOK, gin.H{
		"message":    "",
		"statusCode": 0,
		"data":       lResp,
	})
}

func CreateAccount(c *gin.Context) {
	var uData service.ClientUser
	c.BindJSON(&uData)
	if err := service.CreateUser(uData); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"ok":      false,
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"ok":      true,
		"message": "帳號新增成功",
	})
}

func UpdateAccount(c *gin.Context) {
	var uData service.ClientUser
	c.BindJSON(&uData)
	if err := service.UpdateUser(uData); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"ok":      false,
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"ok":      true,
		"message": "帳號更新成功",
	})
}

func UpdateAccPwd(c *gin.Context) {
	var uData service.ClientUser
	c.BindJSON(&uData)
	if err := service.UpdateUserPwd(uData); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"ok":      false,
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"ok":      true,
		"message": "帳號更新成功",
	})
}

func GetAccount(c *gin.Context) {

	c.JSON(http.StatusOK, gin.H{
		"ok":   true,
		"data": service.GetUserList(),
	})
}

func SrhAccount(c *gin.Context) {

	username := c.Param("username")
	var srh models.User
	srh.Username = username
	dbUser := service.GetUser(srh)

	c.JSON(http.StatusOK, gin.H{
		"message":    "",
		"statusCode": 0,
		"data":       dbUser,
	})
}
