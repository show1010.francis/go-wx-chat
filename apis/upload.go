package apis

import (
	"fmt"
	"go-wx-chat/models"
	"go-wx-chat/service"
	"go-wx-chat/toolkits"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/segmentio/ksuid"
)

type uploadResp struct {
	UrlPath    string `json:"path"`
	UrlFileKey string `json:"fileKey"`
	FilaName   string `json:"originFileName"`
	Sizes      int64  `json:"sizes"`
	Extension  string `json:"extension"`
}

func Upload(c *gin.Context) {
	uid, _ := c.Get("uid")
	uidInt := toolkits.FToint(uid)
	var (
		up             uploadResp
		imageLimitSize int64
	)
	//取得上傳檔案
	file, err := c.FormFile("file")
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message":    "fail",
			"statusCode": 1,
			"data":       up,
		})
		return
	}

	//圖片放置server路徑
	if imageLimitSize, err = strconv.ParseInt(os.Getenv("image_limit"), 10, 64); err != nil {
		imageLimitSize = 500
	}

	// >  KB
	if file.Size/1024 > imageLimitSize {
		c.JSON(http.StatusOK, gin.H{
			"message":    "limit max " + os.Getenv("image_limit") + " KB",
			"statusCode": 1,
			"data":       up,
		})
		return
	}

	//建立圖片唯一碼uuid檔名
	originFile := strings.Split(file.Filename, ".")
	fileKeyUID := ksuid.New().String()
	up.FilaName = fileKeyUID
	if len(originFile) > 0 {
		up.FilaName = fmt.Sprintf("%s.%s", up.FilaName, originFile[len(originFile)-1])
	}
	//圖片放置server路徑
	upDir := os.Getenv("image_dir")

	// Upload the file to specific dst.
	if err := c.SaveUploadedFile(file, filepath.Join(upDir, up.FilaName)); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message":    "fail",
			"statusCode": 1,
			"data":       up,
		})
		return
	}

	//上傳成功 回應內容，並儲存相關資訊
	up.UrlPath = fmt.Sprintf("%s:%s%s%s", os.Getenv("image_host"), os.Getenv("image_port"), os.Getenv("image_url"), up.FilaName)
	up.FilaName = file.Filename
	up.UrlFileKey = fileKeyUID
	up.Sizes = file.Size
	up.Extension = originFile[len(originFile)-1]

	if err := service.UploadImg(models.UploadFile{
		CreatorId:  uidInt,
		FileKey:    up.UrlFileKey,
		OriginName: up.FilaName,
		UrlPath:    up.UrlPath,
		UrlHost:    os.Getenv("image_host"),
		UrlPort:    fmt.Sprintf(":%s", os.Getenv("image_port")),
		UrlDir:     os.Getenv("image_url"),
		Extension:  originFile[len(originFile)-1],
		Sizes:      file.Size,
	}); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message":    "fail",
			"statusCode": 1,
			"data":       up,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message":    "",
		"statusCode": 0,
		"data":       up,
	})
}

func GetFile(c *gin.Context) {
	filedir := c.Param("filedir")
	filename := c.Param("filename")
	c.File(fmt.Sprintf("uploadFile/image/%s/%s", filedir, filename))
}
