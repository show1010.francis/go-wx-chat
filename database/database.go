package database

import (
	"database/sql"
	"fmt"
	"go-wx-chat/toolkits"
	"os"

	"github.com/didi/gendry/manager"
	_ "github.com/go-sql-driver/mysql"
)

type MyDB struct {
	*sql.DB
}

func OpenDB(config toolkits.ConnectionConfig) (*MyDB, error) {
	db, err := manager.New(config.Dbname, config.User, config.Pass, config.Host).Set(
		manager.SetCharset("utf8"),
		manager.SetAllowCleartextPasswords(true),
		manager.SetParseTime(true),
	).Port(3306).Open(true)
	// db, err := sql.Open(config.Dialect, config.URI())

	// db.SetMaxOpenConns(20)
	// db.SetMaxIdleConns(10)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return &MyDB{db}, nil
}

func Open() *MyDB {
	var db *MyDB
	var err error

	db, err = OpenDB(toolkits.ConnectionConfig{
		Dialect: "mysql",
		Host:    os.Getenv("mysql_host"),
		User:    os.Getenv("mysql_user"),
		Pass:    os.Getenv("mysql_password"),
		Dbname:  os.Getenv("mysql_name"),
	})

	fmt.Println("db err", err)
	if err != nil {
		panic(err)
	}

	return db
}
