package main

import (
	"context"
	"go-wx-chat/apis"
	"go-wx-chat/database"
	"go-wx-chat/middleware"
	"go-wx-chat/models"
	"go-wx-chat/pkg/fcm"
	"go-wx-chat/service"
	"go-wx-chat/toolkits"
	"go-wx-chat/wxConn"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db := database.Open()
	defer db.Close()

	fcm.Start() //firebase connetion

	router := GinEngine(db)

	var (
		listenPort = ":" + os.Getenv("http_port")
	)

	srv := &http.Server{
		Addr:         listenPort,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      router,
	}

	go func() {
		log.Println("Server Start on ", listenPort)
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			log.Println(err.Error())
		} else {
			log.Println("Server Closed.")
		}
		// if err := srv.ListenAndServeTLS("./rootCA.pem",
		// 	"./rootCA-key.pem"); err != http.ErrServerClosed {
		// 	log.Println(err.Error())
		// } else {
		// 	log.Println("Server Closed.")
		// }
	}()

	// intercept interrupt
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown ,err=", err)
	} else {
		log.Println("Server Shutdown ...")
	}
}

func initLogFile() {
	f, _ := os.OpenFile("log/"+time.Now().Format("20060102")+"error.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	gin.DefaultWriter = io.MultiWriter(f)
}

func initUserDao(db *database.MyDB) {
	models.MyUserDo = models.NewUserDao(db.DB)
}

func initRoomDao(db *database.MyDB) {
	models.MyRoomDo = models.NewRoomDao(db.DB)
}

func initUploadDao(db *database.MyDB) {
	models.MyUploadDo = models.NewUploadDao(db.DB)
}

func GinEngine(db *database.MyDB) *gin.Engine {
	router := gin.New()
	router.MaxMultipartMemory = 2 << 20 // 1 MiB

	initLogFile()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	//init database Dao
	initUserDao(db)
	initRoomDao(db)
	initUploadDao(db)

	router.Use(middleware.CorsClose())

	var offLine = make(chan service.UserMsg)
	go service.OffLineMsgRecord(offLine)

	//all user websocket connetion mannger
	var cWSConn = wxConn.NewWSMSGManage(offLine)
	go cWSConn.MSGDispatch()

	router.GET("/online-user", func(c *gin.Context) {
		var nowUser []string
		for _, client := range cWSConn.AllToken {
			nowUser = append(nowUser, client.User)
		}
		c.JSON(http.StatusOK, nowUser)
	})

	//serve static html
	router.StaticFS("/web", http.Dir("./website"))

	//serve static html
	router.StaticFS("/dist", http.Dir("./website/dist"))

	//static serve
	router.Use(static.Serve("/chat", static.LocalFile("./website", true)))
	//firebase js
	router.StaticFile("/firebase-messaging-sw.js", "./website/firebase-messaging-sw.js")

	//登入
	router.POST("/php-chat/public/user/login", apis.CheckAndToken)

	//查詢帳號
	router.GET("/php-chat/public/user/search/:username", apis.SrhAccount)

	//上傳檔案
	router.POST("/uploadFile", apis.Upload)

	//下載檔案
	router.GET("/image/:filedir/:filename", apis.GetFile)

	apiAuth := router.Group("/auth")
	apiAuth.Use(middleware.TokenCheck())
	{
		apiAuth.GET("/user", apis.GetAccount)
		//新增帳號
		apiAuth.POST("/user", apis.CreateAccount)
		//更新帳號
		apiAuth.PUT("/user", apis.UpdateAccount)
		//更新密碼
		apiAuth.PUT("/userpwd", apis.UpdateAccPwd)

		//新增好友
		apiAuth.POST("/friend", apis.AddFriend)
		//查詢好友
		apiAuth.GET("/friend", apis.FindFriendList)
		//刪好友
		apiAuth.POST("/friend/del", apis.DelFriend)

		//firebase 前端 註冊裝置後將裝置token傳至server save
		apiAuth.POST("/google/fcm/device", apis.AddDevice)

		//firebase 刪除 裝置 不在接收推播
		apiAuth.POST("/google/fcm/device/del", apis.DelUserDevice)
	}

	//chat websocket
	router.GET("/ws", func(c *gin.Context) {
		tokeID, _ := c.GetQuery("token")

		token, err := models.ParseToken(tokeID)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": err})
			return
		}

		if userTokenM, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			if userTokenM["ID"] == "" {
				c.AbortWithStatus(http.StatusUnauthorized)
				return
			}
			c.Set("TokenSelf", token.Raw)
			c.Set("JWT_PAYLOAD", userTokenM)
			c.Set("user", userTokenM["user"])
			c.Set("uid", userTokenM["uid"])
		} else {
			log.Printf("err %v", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		u, _ := c.Get("user")
		uid, _ := c.Get("uid")

		uidInt := toolkits.FToint(uid)

		wxConn.Wshandler(c.Writer, c.Request, u.(string), cWSConn, uidInt, tokeID)
	})

	return router
}
