package middleware

import (
	"go-wx-chat/models"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type authError struct {
	Message string `json:"error"`
}

func TokenCheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var errMsg authError
		if _, ok := c.Request.Header["Authorization"]; !ok {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"statusCode": 1401,
				"message":    "",
				"data":       errMsg})
			return
		}
		if !strings.Contains(c.Request.Header["Authorization"][0], "Bearer") {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"statusCode": 1401,
				"message":    "",
				"data":       errMsg})
			return
		}
		//e.g:Authorization:Bearer abcdefg
		auth := strings.Split(c.Request.Header["Authorization"][0], " ")

		if len(auth) != 2 {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"statusCode": 1401,
				"message":    "",
				"data":       errMsg})
			return
		}
		var tokeID string
		if tokeID = auth[1]; tokeID == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"statusCode": 1401,
				"message":    "",
				"data":       errMsg})
			return
		}
		token, err := models.ParseToken(tokeID)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"statusCode": 1401,
				"message":    err.Error(),
				"data":       errMsg})
			return
		}

		if userTokenM, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			if userTokenM["ID"] == "" {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"statusCode": 1401,
					"message":    "",
					"data":       errMsg})
				return
			}
			c.Set("TokenSelf", token.Raw)
			c.Set("JWT_PAYLOAD", userTokenM)
			c.Set("user", userTokenM["user"])
			c.Set("gid", userTokenM["gid"])
			c.Set("uid", userTokenM["uid"])
		} else {
			log.Printf("err %v", err)
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"statusCode": 1401,
				"message":    "",
				"data":       errMsg})
			return
		}

		c.Next()
	}
}
