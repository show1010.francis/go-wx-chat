package models

const (
	ErrAccOrPwd                = "帳號或密碼錯誤"
	ErrAccAuthorizationStopped = "帳號權限停用"
	ErrWebConcolseNotAuth      = "無主控台選單權限，登入失敗"
	ErrCreateAccPwdIsNull      = "帳號密碼不可以為空白"
	ErrCreateGidIsNull         = "角色權限不可以為空白"
	ErrCreateAccAlready        = "帳號己經存在"
	ErrCreateFail              = "帳號新增失敗"
	ErrAccNotFound             = "帳號不存在"
	ErrUpdateFail              = "帳號資料更新失敗"
	ErrUpdatePWDFail           = "密碼更新失敗"
	ErrOpCreateIDCardNull      = "身份證不能為空白"
	ErrOpCreateRbLoginFail     = "Server登入失敗"
	ErrOpCreateIDCardSrhFial   = "身份證字號查詢失敗"
	ErrContentNullFial         = "內容不可為空"
	ErrImageNotB64Fial         = "圖片錯誤"
	ErrAddFriendSame           = "不可加自己為好友"
	ErrAddFriendIsNull         = "加好友帳號不可為空"
	ErrAddFriendAlready        = "加好友帳號己經存在"
	ErrAddFriendFail           = "加好友失敗"
)
