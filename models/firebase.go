package models

import (
	"errors"
	"log"
	"time"

	"github.com/didi/gendry/builder"
	qb "github.com/didi/gendry/builder"
	"github.com/didi/gendry/scanner"
)

type DeviceClient struct {
	DeviceToken string `json:"deviceToken" ddb:"device_token"`
	UserId      int64  `json:"userId" ddb:"user_id"` //userAutoId
	DeviceType  int    `json:"deviceType" ddb:"device_type"`
	CreatedAt   int64  `json:"-" ddb:"created_at"`
	DeletedAt   int64  `json:"-" ddb:"deleted_at"`
}

func (this *userDao) FcmDeviceInsert(d DeviceClient) (id int64, err error) {
	table := "fcm_device"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"device_token": d.DeviceToken,
		"user_id":      d.UserId,
		"device_type":  d.DeviceType,
		"created_at":   time.Now().Unix(),
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("userDao db FcmDeviceInsert BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("userDao db FcmDeviceInsert insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("userDao db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *userDao) FcmDeviceUpdate(d DeviceClient) (err error) {
	table := "fcm_device"
	where := map[string]interface{}{}

	if d.DeviceToken != "" {
		where["device_token ="] = d.DeviceToken
	}

	if len(where) == 0 {
		return errors.New("not have where")
	}

	upd := map[string]interface{}{}
	if d.DeletedAt != 0 {
		upd["deleted_at"] = d.DeletedAt
	}

	cond, vals, err := qb.BuildUpdate(table, where, upd)
	if err != nil {
		log.Printf("userDao db BuildUpdate ErrMsg：%v \n", err)
		return
	}
	_, err = this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("userDao db Update ErrMsg：%v \n", err)
	}
	return
}

func (this *userDao) FcmDeviceDelete(deviceToken string) (err error) {

	_, err = this.db.Exec(`delete from fcm_device where device_token=?`, deviceToken)
	if err != nil {
		log.Printf("userDao db delete ErrMsg：%v \n", err)
	}
	return
}

func (this *userDao) FindUserFcmDevice(userId int64) (dList []DeviceClient, err error) {
	table := "fcm_device"
	where := map[string]interface{}{}

	where["user_id"] = userId
	where["deleted_at"] = 0

	selectFields := []string{"device_token", "user_id", "device_type"}
	cond, values, err := builder.BuildSelect(table, where, selectFields)
	if err != nil {
		log.Printf("userDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("userDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &dList); err != nil {
		log.Printf("userDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}

func (this *userDao) FindFcmDeviceToken(deviceToken string) (d DeviceClient, err error) {
	table := "fcm_device"
	where := map[string]interface{}{}

	where["device_token"] = deviceToken
	where["deleted_at"] = 0

	selectFields := []string{"device_token", "user_id", "device_type"}
	cond, values, err := builder.BuildSelect(table, where, selectFields)
	if err != nil {
		log.Printf("userDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("userDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &d); err != nil {
		log.Printf("userDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}
