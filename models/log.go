package models

import qb "github.com/didi/gendry/builder"

type LogWx struct {
	ID      int64  `json:"id"`
	UserId  int64  `json:"userId"`
	Action  int64  `json:"action"`
	Message []byte `json:"message"`
}

var excludeContent = map[string]bool{
	`{"action":"ping"}`: true,
	`{"statusCode":0}`:  true,
}

func (this *roomDao) LogInsert(log LogWx) {
	if _, have := excludeContent[string(log.Message)]; have {
		return
	}
	table := "log_wx"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"userId":  log.UserId,
		"action":  log.Action,
		"message": log.Message,
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		return
	}
	_, err = this.db.Exec(cond, vals...)
	if err != nil {
		return
	}
}
