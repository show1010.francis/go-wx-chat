package models

import (
	"errors"
	"log"
	"time"

	"github.com/didi/gendry/builder"
	qb "github.com/didi/gendry/builder"
	"github.com/didi/gendry/scanner"
)

type RoomMessage struct {
	ID         int64  `json:"messageId" ddb:"id"`
	SenderID   int64  `json:"userId" ddb:"sender_id"`
	TextKind   int    `json:"text_kind" ddb:"text_kind"`
	Text       string `json:"text" ddb:"text"`
	ReadCnt    int64  `json:"readCount"`
	RoomID     int64  `json:"roomId" ddb:"room_id"`
	Device     string `json:"deviceKey" ddb:"device_key"`
	CreateTime int64  `json:"timestamp" ddb:"created_at"`
	CreateDate string `json:"-"`
}

func (this *roomDao) MessageInsert(rm RoomMessage) (int64, error) {
	table := "room_message"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"sender_id":  rm.SenderID,
		"text_kind":  rm.TextKind,
		"text":       rm.Text,
		"room_id":    rm.RoomID,
		"created_at": rm.CreateTime,
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("roomDao db MessageInsert BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("MessageInsert db insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("MessageInsert db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *roomDao) MessageHistory(messageId int64, roomId int64, createdTime int64) (rmList []RoomMessage, err error) {
	table := "room_message"
	selectFields := []string{"id", "sender_id", "text_kind", "text", "room_id", "created_at"}
	where := map[string]interface{}{}
	if messageId != 0 {
		where["id"] = messageId
	}
	if roomId != 0 {
		where["room_id"] = roomId
	}
	if createdTime > 0 {
		where["created_at >"] = createdTime
	}
	cond, values, err := builder.BuildSelect(table, where, selectFields)

	if err != nil {
		log.Printf("roomDao db MessageHistory BuildSelect ErrMsg：%v \n", err)
		return
	}
	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("roomDao db MessageHistory ErrMsg：%v \n", err)
		return nil, err
	}
	defer rows.Close()
	if scanner.Scan(rows, &rmList); err != nil {
		log.Printf("roomDao db scan ErrMsg：%v \n", err)
		return
	}
	return

}

func (this *roomDao) ReceiveMessageInsert(rm RoomMessage) (id int64, err error) {
	table := "receive_message"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"room_message_id": rm.ID,
		"user_id":         rm.SenderID,
		"device_key":      rm.Device,
		"created_at":      time.Now().Unix(),
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("userDao db BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("ReceiveMessageInsert db insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("ReceiveMessageInsert db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

type OffLineRoomMessage struct {
	RoomId  int           `json:"roomId"`
	Message []RoomMessage `json:"message"`
}

func (this *roomDao) GetNotReceveMsgByUserId(userId int64) ([]OffLineRoomMessage, error) {
	rows, err := this.db.Query(`
	select 
	msg.room_id,
	msg.id,msg.sender_id,
	msg.text_kind,msg.text,msg.created_at
	from 
	room_message msg left join room_member rm on msg.room_id = rm.room_id
	left join receive_message rece on rece.room_message_id=msg.id and rece.user_id = rm.user_id
	where rm.user_id=? and rm.status = 1 and rece.id is null
	order by msg.id asc;  `, userId)
	if err != nil {
		log.Printf("roomDao db GetNotReceveMsgByUserId ErrMsg：%v \n", err)
		return nil, err
	}
	defer rows.Close()

	var rList = make([]OffLineRoomMessage, 0)
	for rows.Next() {
		r := new(OffLineRoomMessage)
		rm := new(RoomMessage)
		err := rows.Scan(&r.RoomId,
			&rm.ID, &rm.SenderID,
			&rm.TextKind, &rm.Text, &rm.CreateTime)

		if err != nil {
			log.Printf("roomDao db GetNotReceveMsgByUserId  ErrMsg：%s \n", err)
			return nil, err
		}
		r.Message = append(r.Message, *rm)
		rList = append(rList, *r)
	}

	return rList, err
}

type OffLineMsg struct {
	ID            int64  `json:"id" ddb:"id"`
	ReceiveUserId int64  `json:"userId" ddb:"userId"`
	StatusCode    int    `json:"statusCode" ddb:"status_code"`
	Message       []byte `json:"message" ddb:"message_text"`
	CreateDate    string `json:"-"`
}

func (this *roomDao) OffMsgInsert(om OffLineMsg) (id int64, err error) {
	table := "offline_message"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"userId":       om.ReceiveUserId,
		"status_code":  om.StatusCode,
		"message_text": om.Message,
		"created_at":   time.Now().Unix(),
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("roomDao db OffMsgInsert BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("OffMsgInsert db insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("OffMsgInsert db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *roomDao) OffLineUpdateTag(id int64, userId int64, statusCode int) (err error) {
	table := "offline_message"
	where := map[string]interface{}{}

	if id != 0 {
		where["id"] = id
	}
	if userId != 0 {
		where["userId"] = userId
	}
	if statusCode != 0 {
		where["status_code"] = statusCode
	}
	if len(where) == 0 {
		return errors.New("not have where")
	}

	upd := map[string]interface{}{}
	upd["deleated_at"] = time.Now().Unix()

	cond, vals, err := qb.BuildUpdate(table, where, upd)
	if err != nil {
		log.Printf("roomDao db BuildUpdate ErrMsg：%v \n", err)
		return
	}
	_, err = this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("roomDao db Update ErrMsg：%v \n", err)
	}
	return
}

func (this *roomDao) FindOffLine(userId int64, statusCode int) (om []OffLineMsg, err error) {
	table := "offline_message"
	where := map[string]interface{}{}

	where["userId"] = userId
	where["status_code"] = statusCode
	where["deleated_at"] = builder.IsNull
	where["_orderby"] = "created_at asc"

	selectFields := []string{"id", "userId", "status_code", "message_text", "created_at"}
	cond, values, err := builder.BuildSelect(table, where, selectFields)

	if err != nil {
		log.Printf("roomDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("roomDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &om); err != nil {
		log.Printf("roomDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}

//ReadMessage 已讀訊息
type ReadMessage struct {
	MessageId int64 `json:"messageId" ddb:"room_message_id"`
	ReadUser  int64 `json:"readUser" ddb:"user_id"`
	ReadCnt   int64 `json:"readCount"`
}

func (this *roomDao) MsgReadInsert(messageId int64, readUser int64) (id int64, err error) {
	table := "read_message"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"room_message_id": messageId,
		"user_id":         readUser,
		"created_at":      time.Now().Unix(),
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("roomDao db ProcessMsgInsert BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("ProcessMsgInsert db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *roomDao) GetReadCountById(startMessageId int64, endMessageId int64) ([]ReadMessage, error) {
	rows, err := this.db.Query(`
	select 
	msg.id,count(rd.id) as cnt 
	from room_message msg left join read_message rd on msg.id = rd.room_message_id
	where msg.id >= ? and msg.id<=?
	group by msg.id;  `, startMessageId, endMessageId)
	if err != nil {
		log.Printf("roomDao db GetReadCountById ErrMsg：%v \n", err)
		return nil, err
	}
	defer rows.Close()

	var rList = make([]ReadMessage, 0)
	for rows.Next() {
		r := new(ReadMessage)
		err := rows.Scan(&r.MessageId, &r.ReadCnt)

		if err != nil {
			log.Printf("roomDao db GetReadCountById  ErrMsg：%s \n", err)
			return nil, err
		}
		rList = append(rList, *r)
	}

	return rList, err
}
