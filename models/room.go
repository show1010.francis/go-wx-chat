package models

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/didi/gendry/builder"
	qb "github.com/didi/gendry/builder"
	"github.com/didi/gendry/scanner"
)

const (
	PrivateRoom = 1
	GroupRoom   = 2
)

type Room struct {
	ID                int64  `json:"id" ddb:"id"`
	CreatedByUserId   int64  `json:"created_by_user_id" ddb:"created_by_user_id"`
	Name              string `json:"name" ddb:"name"`
	Type              int    `json:"type" ddb:"room_type"`
	CreateTime        int64  `json:"createTime" ddb:"-"`
	CreateDate        string `json:"-" ddb:"created_at"`
	LastSendTimeStamp int64  `json:"lastTimestamp" ddb:"-"`
}

var (
	MyRoomDo *roomDao
)

type roomDao struct {
	db *sql.DB
}

func NewRoomDao(db *sql.DB) *roomDao {
	return &roomDao{
		db: db,
	}
}

func (this *roomDao) RoomInsert(r Room) (id int64, err error) {
	table := "room"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"name":               r.Name,
		"room_type":          r.Type,
		"created_by_user_id": r.CreatedByUserId,
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("roomDao db RoomInsert BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("RoomInsert db insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("RoomInsert db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *roomDao) RoomUpdate(r Room) (err error) {

	table := "room"
	where := map[string]interface{}{
		"id =": r.ID,
	}
	upd := map[string]interface{}{}
	if r.Name != "" {
		upd["name"] = r.Name
	}
	if r.Type != 0 {
		upd["room_type"] = r.Type
	}
	if r.CreatedByUserId != 0 {
		upd["created_by_user_id"] = r.CreatedByUserId
	}

	cond, vals, err := qb.BuildUpdate(table, where, upd)
	if err != nil {
		log.Printf("roomDao db BuildUpdate ErrMsg：%v \n", err)
		return
	}
	_, err = this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("roomDao db Update ErrMsg：%v \n", err)
	}

	return err
}

func (this *roomDao) FindRoom(id int64) (r Room, err error) {
	table := "room"
	where := map[string]interface{}{}

	where["id"] = id

	selectFields := []string{"name", "room_type", "created_by_user_id", "created_at"}
	cond, values, err := builder.BuildSelect(table, where, selectFields)
	if err != nil {
		log.Printf("roomDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("roomDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &r); err != nil {
		log.Printf("roomDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}

func (this *roomDao) FindRoomList() (r []Room, err error) {
	table := "room"

	selectFields := []string{"name", "room_type", "created_by_user_id"}
	cond, values, err := builder.BuildSelect(table, nil, selectFields)
	if err != nil {
		log.Printf("roomDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("roomDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &r); err != nil {
		log.Printf("roomDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}

type RoomMember struct {
	ID          int64  `json:"-" ddb:"id"`
	UserId      int64  `json:"userId" ddb:"user_id"`
	UserName    string `json:"userName" ddb:"username"`
	UserImg     string `json:"headImg" ddb:"head_img"`
	Status      int    `json:"-" ddb:"status"`
	RoomId      int64  `json:"roomId" ddb:"room_id"`
	ReadedMaxId int64  `json:"-" ddb:"readed_max"`
}

func (this *roomDao) RMemberInsert(m RoomMember) (id int64, err error) {
	table := "room_member"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"user_id": m.UserId,
		"room_id": m.RoomId,
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("roomDao db RMemberInsert BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("RMemberInsert db insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("RMemberInsert db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *roomDao) RMemberUpdate(m RoomMember) (err error) {
	table := "room_member"
	where := map[string]interface{}{
		"user_id =": m.UserId,
		"room_id =": m.RoomId,
	}
	upd := map[string]interface{}{}
	if m.Status != 0 {
		upd["status"] = m.Status
	}
	if m.ReadedMaxId != 0 {
		upd["readed_max"] = m.ReadedMaxId
	}

	cond, vals, err := qb.BuildUpdate(table, where, upd)
	if err != nil {
		log.Printf("RMemberUpdate db BuildUpdate ErrMsg：%v \n", err)
		return
	}
	_, err = this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("RMemberUpdate db Update ErrMsg：%v \n", err)
	}

	return
}

func (this *roomDao) RMemberFind(roomId int64, userId int64) (rm RoomMember) {
	table := "room_member"
	where := map[string]interface{}{}

	where["room_id"] = roomId

	where["user_id"] = userId

	selectFields := []string{"id", "user_id", "room_id", "readed_max", "status"}
	cond, values, err := builder.BuildSelect(table, where, selectFields)
	if err != nil {
		log.Printf("roomDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("roomDao db RMemberFind ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &rm); err != nil {
		log.Printf("roomDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}

func (this *roomDao) FindPersonalRoom(self int64, anotherId int64) ([]Room, error) {
	rows, err := this.db.Query(`select r.id,r.room_type,r.created_by_user_id 
	from room r left join room_member rm on r.id = rm.room_id 
	where r.room_type = 1 and (rm.user_id = ? or rm.user_id = ?)`, self, anotherId)
	if err != nil {
		log.Printf("roomDao db FindPersonalRoom ErrMsg：%v \n", err)
		return nil, err
	}
	defer rows.Close()

	var rList = make([]Room, 0)
	for rows.Next() {
		r := new(Room)
		err := rows.Scan(&r.ID, &r.Type, &r.CreatedByUserId)

		if err != nil {
			log.Printf("roomDao db FindPersonalRoom  ErrMsg：%s \n", err)
			return nil, err
		}
		rList = append(rList, *r)
	}

	return rList, err
}

//check private exist
func (this *roomDao) FindPersonalExist(self int64) map[int64]bool {
	rows, err := this.db.Query(`select room_id 
	from room r left join room_member rm on r.id = rm.room_id 
	where r.room_type=1 and user_id=? 
	group by room_id`, self)
	if err != nil {
		log.Printf("roomDao db FindPersonalExist ErrMsg：%v \n", err)
		return map[int64]bool{}
	}
	defer rows.Close()
	var dbIdList = make(map[int64]bool)
	for rows.Next() {
		var dbId int64
		err := rows.Scan(&dbId)

		if err != nil {
			log.Printf("roomDao db FindPersonalExist  ErrMsg：%s \n", err)
			return map[int64]bool{}
		}

		dbIdList[dbId] = true
	}

	return dbIdList
}

type RoomMemberList struct {
	RoomId     int64        `json:"roomId"`
	RoomName   string       `json:"roomName"`
	RoomType   int          `json:"type"`
	Members    []RoomMember `json:"members"`
	CreateTime int64        `json:"createTime"`
}

func (this *roomDao) FindAllByRoomId(roomId int64, self int64) (RoomMemberList, error) {

	var param []interface{}
	param = append(param, roomId)

	sql := `select rm.user_id,u.username,IFNULL(u.head_img,''),status 
	from room_member rm left join user u on rm.user_id = u.id 
	where rm.room_id = ? `

	if self != 0 {
		sql = sql + fmt.Sprintf("and rm.user_id <> %v", self)
	}

	rows, err := this.db.Query(sql, roomId)

	if err != nil {
		log.Printf("roomDao db FindAllByRoomId ErrMsg：%v \n", err)
		return RoomMemberList{}, err
	}

	defer rows.Close()
	var rList RoomMemberList
	rList.RoomId = roomId
	for rows.Next() {
		r := new(RoomMember)
		err := rows.Scan(&r.UserId, &r.UserName, &r.UserImg, &r.Status)
		if err != nil {
			log.Printf("roomDao db FindAllByRoomId  ErrMsg：%s \n", err)
			return RoomMemberList{}, err
		}
		rList.Members = append(rList.Members, *r)
	}

	return rList, err
}

func (this *roomDao) GetAllPersonalRoomByUserId(userId int64) ([]Room, error) {
	rows, err := this.db.Query(`
	select r.id,r.name,r.room_type,r.created_by_user_id
	from room r left join room_member rm on r.id = rm.room_id 
	where  rm.user_id = ? and rm.status=1`, userId)
	if err != nil {
		log.Printf("roomDao db GetAllPersonalRoomByUserId ErrMsg：%v \n", err)
		return nil, err
	}
	defer rows.Close()

	var rList = make([]Room, 0)
	for rows.Next() {
		r := new(Room)
		err := rows.Scan(&r.ID, &r.Name, &r.Type, &r.CreatedByUserId)

		if err != nil {
			log.Printf("roomDao db GetAllPersonalRoomByUserId  ErrMsg：%s \n", err)
			return nil, err
		}
		rList = append(rList, *r)
	}

	return rList, err
}

func (this *roomDao) GetRoomMessageLastTime(roomId int64) int64 {
	var lastTime int64
	this.db.QueryRow(`select max(created_at) from room_message where room_id = ?;`,
		roomId).Scan(&lastTime)
	return lastTime
}

func (this *roomDao) FindContactUserId(self int64) ([]RoomMember, error) {

	sql := `select allFriend.user_id from (
		select user_id from room_member where room_id in (
				select room_id from room_member where user_id=? and status=1)
				group by user_id
		union 
		SELECT user_id FROM friend_list where friend_userid=?
		) as allFriend  group by user_id; `

	rows, err := this.db.Query(sql, self, self)

	if err != nil {
		log.Printf("roomDao db FindContactUserId ErrMsg：%v \n", err)
		return []RoomMember{}, err
	}

	defer rows.Close()
	var rList []RoomMember
	for rows.Next() {
		r := new(RoomMember)
		err := rows.Scan(&r.UserId)
		if err != nil {
			log.Printf("roomDao db FindContactUserId  ErrMsg：%s \n", err)
			return []RoomMember{}, err
		}
		rList = append(rList, *r)
	}

	return rList, err
}
