package models

import (
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

var TokenKeyStr = "djk92A0$Q3O81aodfjado@12"
var KeyG = []byte(TokenKeyStr)

func SignToken(userID string, uid int64) (string, error) {
	//Sign Token
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"uid":  uid,
		"user": userID,
		"iat":  time.Now().Unix(),
		"exp":  time.Now().Add(time.Hour * 24).Unix(),
	}).SignedString(KeyG)

	return token, err
}

func ParseToken(tokeStr string) (*jwt.Token, error) {
	//Token parse
	token, err := jwt.Parse(tokeStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return KeyG, nil
	})
	return token, err
}
