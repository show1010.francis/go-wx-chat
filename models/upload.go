package models

import (
	"database/sql"
	"log"

	qb "github.com/didi/gendry/builder"
)

type UploadFile struct {
	ID         int64  `json:"id"`
	CreatorId  int64  `json:"creator_id"`
	FileKey    string `json:"file_key"`
	OriginName string `json:"origin_name"`
	UrlPath    string `json:"url_path"`
	UrlHost    string `json:"url_host"`
	UrlPort    string `json:"url_port"`
	UrlDir     string `json:"url_dir"`
	Extension  string `json:"extension"`
	Sizes      int64  `json:"sizes"`
	CreateTime int64  `json:"up_time"`
}

var (
	MyUploadDo *uploadDao
)

type uploadDao struct {
	db *sql.DB
}

func NewUploadDao(db *sql.DB) *uploadDao {
	return &uploadDao{
		db: db,
	}
}

func (this *uploadDao) UploadImgInsert(up UploadFile) (id int64, err error) {
	table := "img_file"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"creator_id":  up.CreatorId,
		"file_key":    up.FileKey,
		"origin_name": up.OriginName,
		"url_path":    up.UrlPath,
		"extension":   up.Extension,
		"sizes":       up.Sizes,
		"url_host":    up.UrlHost,
		"url_port":    up.UrlPort,
		"url_dir":     up.UrlDir,
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("uploadDao db BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("uploadDao db insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("uploadDao db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}
