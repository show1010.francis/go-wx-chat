package models

import (
	"database/sql"
	"errors"
	"log"
	"time"

	"github.com/didi/gendry/builder"
	qb "github.com/didi/gendry/builder"
	"github.com/didi/gendry/scanner"
)

//tag ddb is db column name
type User struct {
	ID           int64     `json:"userId" ddb:"id"`
	Username     string    `json:"username" ddb:"username"`              //帳號
	PasswordHash string    `json:"-" ddb:"password_hash"`                //密碼
	Nickname     string    `json:"nickname" ddb:"nickname"`              //帳號名稱
	HeadImg      string    `json:"headImg" ddb:"head_img"`               //圖像
	CreateTime   int64     `json:"dateOfRegistration" ddb:"create_time"` //註冊日期
	CreatedAt    time.Time `json:"-"`
	UpdatedAt    time.Time `json:"-"`
}

var (
	MyUserDo *userDao
)

type userDao struct {
	db *sql.DB
}

func NewUserDao(db *sql.DB) *userDao {
	return &userDao{
		db: db,
	}
}

func (this *userDao) Insert(u User) (id int64, err error) {
	table := "user"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"username":      u.Username,
		"password_hash": u.PasswordHash,
		"nickname":      u.Nickname,
		"create_time":   time.Now().Unix(),
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("userDao db BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("userDao db insert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("userDao db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *userDao) Update(u User) (err error) {
	table := "user"
	where := map[string]interface{}{}

	if u.Username != "" {
		where["username ="] = u.Username
	}
	if u.ID != 0 {
		where["id ="] = u.ID
	}
	if len(where) == 0 {
		return errors.New("not have where")
	}

	upd := map[string]interface{}{}
	if u.Nickname != "" {
		upd["nickname"] = u.Nickname
	}
	if u.PasswordHash != "" {
		upd["password_hash"] = u.PasswordHash
	}
	if len(u.HeadImg) != 0 {
		upd["head_img"] = u.HeadImg
	}

	cond, vals, err := qb.BuildUpdate(table, where, upd)
	if err != nil {
		log.Printf("userDao db BuildUpdate ErrMsg：%v \n", err)
		return
	}
	_, err = this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("userDao db Update ErrMsg：%v \n", err)
	}
	return
}

func (this *userDao) FindUser(srh User) (u User) {
	table := "user"
	where := map[string]interface{}{}
	if srh.ID != 0 {
		where["id"] = srh.ID
	}
	if srh.Username != "" {
		where["username"] = srh.Username
	}
	selectFields := []string{"id", "username", "nickname", "password_hash", "head_img", "create_time"}
	cond, values, err := builder.BuildSelect(table, where, selectFields)
	if err != nil {
		log.Printf("userDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("userDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &u); err != nil {
		log.Printf("userDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}

func (this *userDao) FindUserList() (uList []User, err error) {
	table := "user"
	selectFields := []string{"id", "username", "nickname", "password_hash", "head_img", "create_time"}
	cond, values, err := builder.BuildSelect(table, nil, selectFields)
	if err != nil {
		log.Printf("userDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("userDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &uList); err != nil {
		log.Printf("userDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}

type UserFriend struct {
	ID        int64  `json:"id" ddb:"id"`
	UserId    int64  `json:"userId" ddb:"user_id"`
	FriendId  int64  `json:"friendUserId" ddb:"friend_userid"`
	AddTime   int64  `json:"addtime" ddb:"add_time"`
	DelTime   int64  `json:"-" ddb:"del_time"`
	BlockTime int64  `json:"-" ddb:"block_time"`
	Username  string `json:"username" ddb:"username"` //帳號
	Nickname  string `json:"nickname" ddb:"nickname"` //帳號名稱
	HeadImg   string `json:"headImg" ddb:"head_img"`  //圖像
}

func (this *userDao) FriendInsert(uf UserFriend) (id int64, err error) {
	table := "friend_list"
	var insertData []map[string]interface{}
	insertData = append(insertData, map[string]interface{}{
		"user_id":       uf.UserId,
		"friend_userid": uf.FriendId,
		"add_time":      uf.AddTime,
	})

	cond, vals, err := qb.BuildInsert(table, insertData)
	if err != nil {
		log.Printf("userDao db BuildInsert ErrMsg：%v \n", err)
		return 0, err
	}
	Result, err := this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("userDao db FriendInsert ErrMsg：%v \n", err)
		return 0, err
	}

	lastAutoID, err := Result.LastInsertId()
	if err != nil {
		log.Printf("userDao db lastAutoID ErrMsg:%v \n", err)
		return 0, err
	}

	return lastAutoID, err
}

func (this *userDao) FriendUpdate(uf UserFriend) (err error) {
	table := "friend_list"
	where := map[string]interface{}{}

	if uf.ID != 0 {
		where["id"] = uf.ID
	}

	if len(where) == 0 {
		return errors.New("not have where")
	}

	upd := map[string]interface{}{}
	if uf.DelTime != 0 {
		if uf.DelTime == -1 {
			upd["del_time"] = 0
		} else {
			upd["del_time"] = uf.DelTime
		}
	}
	if uf.BlockTime != 0 {
		upd["block_time"] = uf.BlockTime
	}

	cond, vals, err := qb.BuildUpdate(table, where, upd)
	if err != nil {
		log.Printf("userDao db BuildUpdate ErrMsg：%v \n", err)
		return
	}

	_, err = this.db.Exec(cond, vals...)
	if err != nil {
		log.Printf("userDao db FriendUpdate ErrMsg：%v \n", err)
	}
	return
}

func (this *userDao) FindFriendList(userId int64) ([]UserFriend, error) {
	rows, err := this.db.Query(`
	SELECT 
	fd.id,fd.user_id,fd.friend_userid,fd.add_time,u.username,u.nickname,IFNULL(u.head_img,'') 
	FROM friend_list fd left join user u on fd.friend_userid=u.id 
	where fd.user_id=? and del_time=0; `, userId)
	if err != nil {
		log.Printf("userDao db FindFriendList ErrMsg：%v \n", err)
		return nil, err
	}
	defer rows.Close()

	var rList = make([]UserFriend, 0)
	for rows.Next() {
		r := new(UserFriend)
		err := rows.Scan(&r.ID, &r.UserId, &r.FriendId, &r.AddTime, &r.Username, &r.Nickname, &r.HeadImg)

		if err != nil {
			log.Printf("userDao db FindFriendList  ErrMsg：%s \n", err)
			return nil, err
		}
		rList = append(rList, *r)
	}

	return rList, err
}

func (this *userDao) FindFriend(userId int64, friendId int64) (uf UserFriend) {
	table := "friend_list"
	where := map[string]interface{}{}

	where["user_id"] = userId

	where["friend_userid"] = friendId

	selectFields := []string{"id", "user_id", "friend_userid", "add_time", "del_time", "block_time"}
	cond, values, err := builder.BuildSelect(table, where, selectFields)
	if err != nil {
		log.Printf("userDao db BuildSelect ErrMsg：%v \n", err)
		return
	}

	rows, err := this.db.Query(cond, values...)
	if err != nil {
		log.Printf("userDao db Query ErrMsg：%v \n", err)
		return
	}
	defer rows.Close()
	if scanner.Scan(rows, &uf); err != nil {
		log.Printf("userDao db scan ErrMsg：%v \n", err)
		return
	}

	return
}
