package fcm

import (
	"fmt"
	"log"
	"os"

	"github.com/appleboy/go-fcm"
)

var (
	FcmClinet *fcm.Client
)

type FcmSendBody struct {
	DeviceToken string
	DeviceType  int
	Title       string
	Content     string
	RoomId      int64
	RoomType    int
	SendUser    int64
}

func Start() {
	var err error
	// Create a FCM client to send the message.
	FcmClinet, err = fcm.NewClient(os.Getenv("fcmtoken"))
	if err != nil {
		log.Fatalln(err)
	}
}

func Send(sendData FcmSendBody) bool {

	var clickUrl string
	if sendData.DeviceType == 1 {
		clickUrl = "http://124.108.169.162:8001/chat"
	}

	// Create the message to be sent.
	msg := &fcm.Message{
		To: sendData.DeviceToken,
		Notification: &fcm.Notification{
			Title:       "chat",
			Body:        "有新的聊天訊息",
			ClickAction: clickUrl,
		},
		Data: map[string]interface{}{
			"deviceToken": sendData.DeviceToken,
			"title":       sendData.Title,
			"body":        sendData.Content,
			"roomId":      sendData.RoomId,
			"roomType":    sendData.RoomType,
			"sendUser":    sendData.SendUser,
		},
	}

	// Send the message and receive the response without retries.
	response, err := FcmClinet.Send(msg)
	if err != nil {
		log.Println(err)
		return false
	}
	fmt.Println(response)
	return response.Success == 1
}
