package service

import (
	"errors"
	"go-wx-chat/models"
)

func CreateUserNewDevice(deviceToken string, userId int64, deviceType int) error {
	deviceC := models.DeviceClient{
		DeviceToken: deviceToken,
		UserId:      userId,
		DeviceType:  deviceType,
	}

	//先確認是否己存在db
	d, _ := models.MyUserDo.FindFcmDeviceToken(deviceToken)

	if d.UserId > 0 {
		return nil
	}

	if _, err := models.MyUserDo.FcmDeviceInsert(deviceC); err != nil {
		return errors.New(models.ErrCreateFail)
	}
	return nil
}

func DelUserDevice(deviceToken string) error {

	if err := models.MyUserDo.FcmDeviceDelete(deviceToken); err != nil {
		return errors.New(models.ErrUpdateFail)
	}
	return nil
}

func FindUserDevice(userId int64) []models.DeviceClient {
	var dList []models.DeviceClient
	var err error

	if dList, _ = models.MyUserDo.FindUserFcmDevice(userId); err != nil {
		return nil
	}

	return dList
}
