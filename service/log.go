package service

import (
	"go-wx-chat/models"
)

const (
	WxCoon   = 1001
	WxClose  = 2001
	WxSend   = 3001
	WxRecive = 4001
)

func LogWx(l models.LogWx) {
	models.MyRoomDo.LogInsert(l)
}
