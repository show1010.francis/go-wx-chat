package service

import (
	"go-wx-chat/models"
)

type UserMsg struct {
	ReceiveUserId int64
	StatusCode    int
	OffLineId     int64
	MessageIds    []int64
	Message       []byte
}

func OffLineMsgRecord(in chan UserMsg) {
	for {
		select {
		case data := <-in:
			om := models.OffLineMsg{
				ReceiveUserId: data.ReceiveUserId,
				StatusCode:    data.StatusCode,
				Message:       data.Message,
			}
			if om.StatusCode == 2031 {
				OffLineMsgInsert(om)
			}
		}
	}
}

func OffLineMsgInsert(om models.OffLineMsg) int64 {
	lastAutoId, _ := models.MyRoomDo.OffMsgInsert(om)
	return lastAutoId
}
