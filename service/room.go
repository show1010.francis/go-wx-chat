package service

import (
	"go-wx-chat/models"
	"go-wx-chat/toolkits"
	"time"
)

func CreatePrivateRoom(roomName string, createUserID int64, otherUserID int64) (int64, error) {

	var newRoom = models.Room{
		CreatedByUserId: createUserID,
		Name:            roomName,
		Type:            models.PrivateRoom,
	}
	var (
		lastRoomId int64
		err        error
	)

	//建立新房間
	if lastRoomId, err = models.MyRoomDo.RoomInsert(newRoom); err != nil {
		return lastRoomId, err
	}

	//建立房間成員-創建者
	if err := RoomAddMember(lastRoomId, createUserID); err != nil {
		return lastRoomId, err
	}
	//建立房間成員-被邀請者
	if err := RoomAddMember(lastRoomId, otherUserID); err != nil {
		return lastRoomId, err
	}

	return lastRoomId, err
}

func GetRoomMember(roomID int64) (models.RoomMemberList, error) {
	roomMemberList, err := models.MyRoomDo.FindAllByRoomId(roomID, 0)
	roomData, err := models.MyRoomDo.FindRoom(roomID)
	//資料庫 時間為+8 所以 轉回utc的unix
	t, err := time.ParseInLocation("2006-01-02 15:04:05", roomData.CreateDate, time.Local)
	roomMemberList.RoomType = roomData.Type
	roomMemberList.RoomName = roomData.Name
	roomMemberList.CreateTime = t.UTC().Unix()
	return roomMemberList, err
}

func GetRoomMemberExceptSelf(roomID int64, user int64) (models.RoomMemberList, error) {
	roomMemberList, err := models.MyRoomDo.FindAllByRoomId(roomID, user)
	return roomMemberList, err
}

func GetRoomBySelf(userID int64) ([]models.Room, error) {
	roomList, err := models.MyRoomDo.GetAllPersonalRoomByUserId(userID)
	for i, room := range roomList {
		lastTime := models.MyRoomDo.GetRoomMessageLastTime(room.ID)
		roomList[i].LastSendTimeStamp = lastTime
	}
	return roomList, err
}

func GetPersonalRoom(userId int64, otherUserId int64) ([]models.Room, error) {
	return models.MyRoomDo.FindPersonalRoom(userId, otherUserId)
}

func CheckPersonExist(userId int64, otherUserId int64) int64 {
	userHaveRommList := models.MyRoomDo.FindPersonalExist(userId)
	otherHaveRommList := models.MyRoomDo.FindPersonalExist(otherUserId)

	for userRoom, _ := range userHaveRommList {
		if _, have := otherHaveRommList[userRoom]; have {
			return userRoom
		}
	}
	return 0
}

//SendMessage 發送訊息
func SendMessage(messageKind int, message string, user int64, roomID int64) (int64, error) {

	sendTime := time.Now().Unix()
	var rm = models.RoomMessage{
		SenderID:   user,
		Text:       message,
		TextKind:   messageKind,
		RoomID:     roomID,
		CreateTime: sendTime,
	}

	//發送訊息寫入
	messageId, err := models.MyRoomDo.MessageInsert(rm)

	return messageId, err
}

type ReadMessage struct {
	MessageId int64 `json:"messageId" `
	ReadCnt   int64 `json:"readCount"`
}

//ReadedMessage 寫入己讀訊息 並獲得最新己讀數量
func ReadedMessage(roomId int64, messageIds []int64, readUser int64) []ReadMessage {

	var rdMsgs []ReadMessage

	//查詢在該聊天室 己讀資訊
	roomMemberInfo := models.MyRoomDo.RMemberFind(roomId, readUser)

	for _, messageId := range messageIds {
		//tag 已讀資訊 小於 紀錄不處理
		if messageId >= roomMemberInfo.ReadedMaxId {
			if messageDatas, _ := models.MyRoomDo.MessageHistory(messageId, 0, 0); len(messageDatas) > 0 {
				//己讀人員 不等於訊息送出人員 則記入己讀
				if messageDatas[0].SenderID != readUser {
					//己讀訊息
					models.MyRoomDo.MsgReadInsert(messageId, readUser)
				}
			}
		}
	}

	minMessageIds, maxMessageIds := toolkits.FindMaxAndMin(messageIds)

	//更新人員 該聊天室 最後己讀index
	models.MyRoomDo.RMemberUpdate(models.RoomMember{
		RoomId:      roomId,
		UserId:      readUser,
		ReadedMaxId: maxMessageIds,
	})

	readMessageList, _ := models.MyRoomDo.GetReadCountById(minMessageIds, maxMessageIds)
	for _, data := range readMessageList {
		rdMsgs = append(rdMsgs, ReadMessage{
			MessageId: data.MessageId,
			ReadCnt:   data.ReadCnt,
		})
	}

	return rdMsgs
}

func CreateGroupRoom(roomName string, createUserID int64, otherUserID []int64) (int64, error) {

	var newRoom = models.Room{
		CreatedByUserId: createUserID,
		Name:            roomName,
		Type:            models.GroupRoom,
	}
	var (
		lastRoomId int64
		err        error
	)

	//建立新房間
	if lastRoomId, err = models.MyRoomDo.RoomInsert(newRoom); err != nil {
		return lastRoomId, err
	}

	//建立房間成員-創建者
	if err := RoomAddMember(lastRoomId, createUserID); err != nil {
		return lastRoomId, err
	}

	for _, memberID := range otherUserID {
		if err := RoomAddMember(lastRoomId, memberID); err != nil {
			return lastRoomId, err
		}
	}

	return lastRoomId, err
}

func RoomAddMember(roomId int64, userId int64) error {

	var addMember = models.RoomMember{
		UserId: userId,
		RoomId: roomId,
	}

	_, err := models.MyRoomDo.RMemberInsert(addMember)

	return err
}

func RoomUpdMember(roomId int64, userId int64, status int) error {

	var updMember = models.RoomMember{
		UserId: userId,
		RoomId: roomId,
		Status: status,
	}

	err := models.MyRoomDo.RMemberUpdate(updMember)

	return err
}

func RoomMessage(roomId int64, createdTime int64) ([]models.RoomMessage, error) {
	messages, err := models.MyRoomDo.MessageHistory(0, roomId, createdTime)
	return messages, err
}

func ReceiveMessageInsert(roomMessageIds []int64, userId int64) error {
	for _, msgId := range roomMessageIds {
		if _, err := models.MyRoomDo.ReceiveMessageInsert(models.RoomMessage{
			ID:       msgId,
			SenderID: userId,
			Device:   "",
		}); err != nil {
			return err
		}
	}
	return nil
}

func GetOffLineMessage(userId int64) ([]models.OffLineRoomMessage, error) {
	return models.MyRoomDo.GetNotReceveMsgByUserId(userId)
}

func OffMsgInsert(om models.OffLineMsg) error {
	_, err := models.MyRoomDo.OffMsgInsert(om)
	return err
}

func FindContactUserId(userId int64) ([]models.RoomMember, error) {
	return models.MyRoomDo.FindContactUserId(userId)
}
