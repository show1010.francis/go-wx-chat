package service

import (
	"go-wx-chat/models"
)

func UploadImg(up models.UploadFile) error {

	_, err := models.MyUploadDo.UploadImgInsert(up)
	return err
}
