package service

import (
	"errors"
	"go-wx-chat/models"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type ClientUser struct {
	LoginAcc string `json:"username"`
	LoginPwd string `json:"password"`
	NickName string `json:"nickname"`
}

func CreateUser(u ClientUser) error {

	var err error
	if strings.TrimSpace(u.NickName) == "" || strings.TrimSpace(u.LoginAcc) == "" || strings.TrimSpace(u.LoginPwd) == "" {
		return errors.New(models.ErrCreateAccPwdIsNull)
	}

	var mUser models.User
	mUser.Username = u.LoginAcc
	mUser.PasswordHash = u.LoginPwd
	mUser.Nickname = u.NickName

	var srhUser models.User
	srhUser.Username = mUser.Username
	dbUser := models.MyUserDo.FindUser(srhUser)
	if dbUser.Username != "" {
		return errors.New(models.ErrCreateAccAlready)
	}

	if mUser.PasswordHash, err = HashPassword(u.LoginPwd); err != nil {
		return errors.New(models.ErrCreateFail)
	}

	if _, err := models.MyUserDo.Insert(mUser); err != nil {
		return errors.New(models.ErrCreateFail)
	}
	return nil

}

func UpdateUserPwd(u ClientUser) error {

	var err error

	if strings.TrimSpace(u.LoginAcc) == "" || strings.TrimSpace(u.LoginPwd) == "" {
		return errors.New(models.ErrCreateAccPwdIsNull)
	}

	var mUser models.User
	mUser.Username = u.LoginAcc
	mUser.PasswordHash = u.LoginPwd
	mUser.Nickname = u.NickName
	var srhUser models.User
	srhUser.Username = mUser.Username
	dbUser := models.MyUserDo.FindUser(srhUser)
	if dbUser.Username == "" {
		return errors.New(models.ErrAccNotFound)
	}

	if mUser.PasswordHash, err = HashPassword(u.LoginPwd); err != nil {
		return errors.New(models.ErrCreateFail)
	}

	if err := models.MyUserDo.Update(mUser); err != nil {
		return errors.New(models.ErrCreateFail)
	}
	return nil

}

func UpdateUser(u ClientUser) error {

	var err error

	if strings.TrimSpace(u.LoginAcc) == "" || strings.TrimSpace(u.NickName) == "" {
		return errors.New(models.ErrCreateAccPwdIsNull)
	}
	var mUser models.User
	mUser.Username = u.LoginAcc
	mUser.Nickname = u.NickName
	var srhUser models.User
	srhUser.Username = mUser.Username
	dbUser := models.MyUserDo.FindUser(srhUser)
	if dbUser.Username == "" {
		return errors.New(models.ErrAccNotFound)
	}

	if err = models.MyUserDo.Update(mUser); err != nil {
		return errors.New(models.ErrUpdateFail)
	}
	return nil

}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err != nil
}

func CheckLogin(loginAcc, loginPwd string) (error, models.User) {
	if loginAcc == "" || loginPwd == "" {
		return errors.New(models.ErrAccOrPwd), models.User{}
	}
	var srhUser models.User
	srhUser.Username = loginAcc
	dbUser := models.MyUserDo.FindUser(srhUser)
	if dbUser.Username == "" {
		return errors.New(models.ErrAccOrPwd), models.User{}
	}

	if CheckPasswordHash(loginPwd, dbUser.PasswordHash) {
		return errors.New(models.ErrAccOrPwd), models.User{}
	}

	return nil, dbUser

}

func GetUserList() []models.User {
	uList, _ := models.MyUserDo.FindUserList()
	return uList
}

func GetUser(srh models.User) models.User {
	return models.MyUserDo.FindUser(srh)
}

func UpdateUserHeadImg(userId int64, img string) error {

	var err error

	if strings.TrimSpace(img) == "" {
		return errors.New(models.ErrContentNullFial)
	}
	var mUser models.User
	mUser.ID = userId
	mUser.HeadImg = img

	if err = models.MyUserDo.Update(mUser); err != nil {
		return errors.New(models.ErrUpdateFail)
	}
	return nil

}

type Friend struct {
	UserId   int64 `json:"-"`
	FriendId int64 `json:"friendUserId"`
}

func AddFriend(af Friend) error {

	if af.UserId == 0 || af.FriendId == 0 {
		return errors.New(models.ErrAddFriendIsNull)
	}
	if af.UserId == af.FriendId {
		return errors.New(models.ErrAddFriendIsNull)
	}

	dbFriend := models.MyUserDo.FindUser(models.User{
		ID: af.FriendId,
	})
	if dbFriend.Username == "" {
		return errors.New(models.ErrAccNotFound)
	}
	mAF := models.UserFriend{
		UserId:   af.UserId,
		FriendId: af.FriendId,
		AddTime:  time.Now().Unix(),
	}

	dbAF := models.MyUserDo.FindFriend(af.UserId, af.FriendId)

	if dbAF.UserId != 0 {
		if dbAF.DelTime == 0 {
			return errors.New(models.ErrAddFriendAlready)
		}
		addData := models.UserFriend{
			ID:      dbAF.ID,
			DelTime: -1,
		}
		models.MyUserDo.FriendUpdate(addData)
		return nil
	}

	if _, err := models.MyUserDo.FriendInsert(mAF); err != nil {
		return errors.New(models.ErrAddFriendFail)
	}
	return nil

}

func FindFriends(userId int64) []models.UserFriend {
	var list = make([]models.UserFriend, 0)
	list, _ = models.MyUserDo.FindFriendList(userId)

	return list

}

func DelFriends(userId int64, friend_id int64) error {
	if userId == 0 || friend_id == 0 {
		return errors.New("data is null")
	}
	dbAF := models.MyUserDo.FindFriend(userId, friend_id)
	if dbAF.ID == 0 {
		return errors.New(models.ErrContentNullFial)
	}

	delData := models.UserFriend{
		ID:      dbAF.ID,
		DelTime: time.Now().Unix(),
	}
	err := models.MyUserDo.FriendUpdate(delData)

	return err

}
