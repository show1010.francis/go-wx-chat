package toolkits

import (
	"encoding/json"
	"fmt"
)

const MYSQL = "mysql"

type ConnectionConfig struct {
	Dialect  string
	Filepath string //sqlite only
	Host     string
	User     string
	Pass     string
	Dbname   string
}

func (cc ConnectionConfig) URI() (uri string) {

	if cc.Dialect == MYSQL {
		uri = fmt.Sprintf("%s:%s@%s/%s?charset=utf8&parseTime=True&loc=Local", cc.User, cc.Pass, cc.Host, cc.Dbname)
	}

	return
}

func FToint(num interface{}) int64 {
	var numInt int64 = 0
	switch exp := num.(type) {
	case float64:
		numInt = int64(exp)
	case json.Number:
		v, _ := exp.Int64()
		numInt = v
	}
	return numInt
}

func FindMaxAndMin(ints []int64) (min int64, max int64) {
	if len(ints) == 0 {
		return min, max
	}
	min = ints[0]
	max = ints[0]
	for _, num := range ints {
		if num > max {
			max = num
		}
		if num < min {
			min = num
		}
	}
	return min, max
}
