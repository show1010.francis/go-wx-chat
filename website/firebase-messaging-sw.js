importScripts("https://www.gstatic.com/firebasejs/7.18.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.18.0/firebase-messaging.js");
importScripts("https://www.gstatic.com/firebasejs/7.18.0/firebase-analytics.js");

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAWYjPoPGkZejt42hItU3aAG9BooOaeTUQ",
    authDomain: "gochat-509a1.firebaseapp.com",
    databaseURL: "https://gochat-509a1.firebaseio.com",
    projectId: "gochat-509a1",
    storageBucket: "gochat-509a1.appspot.com",
    messagingSenderId: "669448722616",
    appId: "1:669448722616:web:4a459e47400c9035fa3c4d",
    measurementId: "G-WHWNLLZ011"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


// Get the Messaging service for the default app
var defaultMessaging = firebase.messaging();

defaultMessaging.setBackgroundMessageHandler(function(payload) {
    console.log('setBackgroundMessageHandler',payload)
    return self.registration.showNotification(notificationTitle,
      notificationOptions);
});

